﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiGiaoHang.Controllers
{
    public class StatusBillController : ApiController
    {
        // GET: api/StatusBill
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/StatusBill/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/StatusBill
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/StatusBill/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/StatusBill/5
        public void Delete(int id)
        {
        }
    }
}
