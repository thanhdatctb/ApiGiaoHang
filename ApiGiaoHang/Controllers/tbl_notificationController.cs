﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApiGiaoHang.Models;

namespace ApiGiaoHang.Controllers
{
    public class tbl_notificationController : ApiController
    {
        private dataEntities db = new dataEntities();

        // GET: api/tbl_notification
        public IQueryable<tbl_notification> Gettbl_notification()
        {
            return db.tbl_notification;
        }

        // GET: api/tbl_notification/5
        [ResponseType(typeof(tbl_notification))]
        public IHttpActionResult Gettbl_notification(int id)
        {
            tbl_notification tbl_notification = db.tbl_notification.Find(id);
            if (tbl_notification == null)
            {
                return NotFound();
            }

            return Ok(tbl_notification);
        }

        // PUT: api/tbl_notification/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttbl_notification(int id, tbl_notification tbl_notification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_notification.notification_id)
            {
                return BadRequest();
            }

            db.Entry(tbl_notification).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbl_notificationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/tbl_notification
        [ResponseType(typeof(tbl_notification))]
        public IHttpActionResult Posttbl_notification(tbl_notification tbl_notification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tbl_notification.Add(tbl_notification);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tbl_notification.notification_id }, tbl_notification);
        }

        // DELETE: api/tbl_notification/5
        [ResponseType(typeof(tbl_notification))]
        public IHttpActionResult Deletetbl_notification(int id)
        {
            tbl_notification tbl_notification = db.tbl_notification.Find(id);
            if (tbl_notification == null)
            {
                return NotFound();
            }

            db.tbl_notification.Remove(tbl_notification);
            db.SaveChanges();

            return Ok(tbl_notification);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tbl_notificationExists(int id)
        {
            return db.tbl_notification.Count(e => e.notification_id == id) > 0;
        }
    }
}