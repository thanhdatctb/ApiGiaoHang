﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApiGiaoHang.Models;

namespace ApiGiaoHang.Controllers
{
    public class tbl_billController : ApiController
    {
        private dataEntities db = new dataEntities();

        // GET: api/tbl_bill
        public IQueryable<tbl_bill> Gettbl_bill()
        {
            return db.tbl_bill;
        }

        // GET: api/tbl_bill/5
        [ResponseType(typeof(tbl_bill))]
        public IHttpActionResult Gettbl_bill(int id)
        {
            tbl_bill tbl_bill = db.tbl_bill.Find(id);
            if (tbl_bill == null)
            {
                return NotFound();
            }

            return Ok(tbl_bill);
        }

        // PUT: api/tbl_bill/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttbl_bill(int id, tbl_bill tbl_bill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_bill.bill_id)
            {
                return BadRequest();
            }

            db.Entry(tbl_bill).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbl_billExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/tbl_bill
        [ResponseType(typeof(tbl_bill))]
        public IHttpActionResult Posttbl_bill(tbl_bill tbl_bill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tbl_bill.Add(tbl_bill);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tbl_bill.bill_id }, tbl_bill);
        }

        // DELETE: api/tbl_bill/5
        [ResponseType(typeof(tbl_bill))]
        public IHttpActionResult Deletetbl_bill(int id)
        {
            tbl_bill tbl_bill = db.tbl_bill.Find(id);
            if (tbl_bill == null)
            {
                return NotFound();
            }

            db.tbl_bill.Remove(tbl_bill);
            db.SaveChanges();

            return Ok(tbl_bill);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tbl_billExists(int id)
        {
            return db.tbl_bill.Count(e => e.bill_id == id) > 0;
        }
    }
}