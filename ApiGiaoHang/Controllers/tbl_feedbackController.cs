﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApiGiaoHang.Models;

namespace ApiGiaoHang.Controllers
{
    public class tbl_feedbackController : ApiController
    {
        private dataEntities db = new dataEntities();

        // GET: api/tbl_feedback
        public IQueryable<tbl_feedback> Gettbl_feedback()
        {
            return db.tbl_feedback;
        }

        // GET: api/tbl_feedback/5
        [ResponseType(typeof(tbl_feedback))]
        public IHttpActionResult Gettbl_feedback(int id)
        {
            tbl_feedback tbl_feedback = db.tbl_feedback.Find(id);
            if (tbl_feedback == null)
            {
                return NotFound();
            }

            return Ok(tbl_feedback);
        }

        // PUT: api/tbl_feedback/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttbl_feedback(int id, tbl_feedback tbl_feedback)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_feedback.feedback_id)
            {
                return BadRequest();
            }

            db.Entry(tbl_feedback).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbl_feedbackExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/tbl_feedback
        [ResponseType(typeof(tbl_feedback))]
        public IHttpActionResult Posttbl_feedback(tbl_feedback tbl_feedback)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tbl_feedback.Add(tbl_feedback);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tbl_feedback.feedback_id }, tbl_feedback);
        }

        // DELETE: api/tbl_feedback/5
        [ResponseType(typeof(tbl_feedback))]
        public IHttpActionResult Deletetbl_feedback(int id)
        {
            tbl_feedback tbl_feedback = db.tbl_feedback.Find(id);
            if (tbl_feedback == null)
            {
                return NotFound();
            }

            db.tbl_feedback.Remove(tbl_feedback);
            db.SaveChanges();

            return Ok(tbl_feedback);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tbl_feedbackExists(int id)
        {
            return db.tbl_feedback.Count(e => e.feedback_id == id) > 0;
        }
    }
}