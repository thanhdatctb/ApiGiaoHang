﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApiGiaoHang.Models;

namespace ApiGiaoHang.Controllers
{
    public class tbl_customerController : ApiController
    {
        private dataEntities db = new dataEntities();

        // GET: api/tbl_customer
        public IQueryable<tbl_customer> Gettbl_customer()
        {
            return db.tbl_customer;
        }

        // GET: api/tbl_customer/5
        [ResponseType(typeof(tbl_customer))]
        public IHttpActionResult Gettbl_customer(int id)
        {
            tbl_customer tbl_customer = db.tbl_customer.Find(id);
            if (tbl_customer == null)
            {
                return NotFound();
            }

            return Ok(tbl_customer);
        }

        // PUT: api/tbl_customer/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttbl_customer(int id, tbl_customer tbl_customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_customer.customer_id)
            {
                return BadRequest();
            }

            db.Entry(tbl_customer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbl_customerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/tbl_customer
        [ResponseType(typeof(tbl_customer))]
        public IHttpActionResult Posttbl_customer(tbl_customer tbl_customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tbl_customer.Add(tbl_customer);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tbl_customer.customer_id }, tbl_customer);
        }

        // DELETE: api/tbl_customer/5
        [ResponseType(typeof(tbl_customer))]
        public IHttpActionResult Deletetbl_customer(int id)
        {
            tbl_customer tbl_customer = db.tbl_customer.Find(id);
            if (tbl_customer == null)
            {
                return NotFound();
            }

            db.tbl_customer.Remove(tbl_customer);
            db.SaveChanges();

            return Ok(tbl_customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tbl_customerExists(int id)
        {
            return db.tbl_customer.Count(e => e.customer_id == id) > 0;
        }
    }
}