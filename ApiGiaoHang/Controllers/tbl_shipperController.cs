﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApiGiaoHang.Models;

namespace ApiGiaoHang.Controllers
{
    public class tbl_shipperController : ApiController
    {
        private dataEntities db = new dataEntities();

        // GET: api/tbl_shipper
        public IQueryable<tbl_shipper> Gettbl_shipper()
        {
            return db.tbl_shipper;
        }

        // GET: api/tbl_shipper/5
        [ResponseType(typeof(tbl_shipper))]
        public IHttpActionResult Gettbl_shipper(int id)
        {
            tbl_shipper tbl_shipper = db.tbl_shipper.Find(id);
            if (tbl_shipper == null)
            {
                return NotFound();
            }

            return Ok(tbl_shipper);
        }

        // PUT: api/tbl_shipper/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttbl_shipper(int id, tbl_shipper tbl_shipper)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_shipper.shipper_id)
            {
                return BadRequest();
            }

            db.Entry(tbl_shipper).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbl_shipperExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/tbl_shipper
        [ResponseType(typeof(tbl_shipper))]
        public IHttpActionResult Posttbl_shipper(tbl_shipper tbl_shipper)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tbl_shipper.Add(tbl_shipper);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tbl_shipper.shipper_id }, tbl_shipper);
        }

        // DELETE: api/tbl_shipper/5
        [ResponseType(typeof(tbl_shipper))]
        public IHttpActionResult Deletetbl_shipper(int id)
        {
            tbl_shipper tbl_shipper = db.tbl_shipper.Find(id);
            if (tbl_shipper == null)
            {
                return NotFound();
            }

            db.tbl_shipper.Remove(tbl_shipper);
            db.SaveChanges();

            return Ok(tbl_shipper);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tbl_shipperExists(int id)
        {
            return db.tbl_shipper.Count(e => e.shipper_id == id) > 0;
        }
    }
}